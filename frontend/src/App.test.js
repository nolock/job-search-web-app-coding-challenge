import React from 'react';
import ReactDOM from 'react-dom';
import Homepage from './components/Homepage';
import JobAdRow from './components/JobAdRow';
import JobAdNewSuccess from './components/JobAdNewSuccess';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Homepage />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing', () => {
  const div = document.createElement('div');

  const data = {
    id: 1,
    title: 'Title',
    description: 'description',
    location: 'location',
    user:{
      name: 'Test',
      created: 'Date',
    }
  };

  ReactDOM.render(<JobAdRow data={data} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(<JobAdNewSuccess />, div);
  ReactDOM.unmountComponentAtNode(div);
});


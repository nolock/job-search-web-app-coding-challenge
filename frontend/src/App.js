import React from 'react';
import './App.css';

export class Auth{
    static  user(){
        return JSON.parse(sessionStorage.getItem('user'));
    }

    static token(){
        return sessionStorage.getItem('apiToken');
    }

    static purge(){
        sessionStorage.clear();
    }
}

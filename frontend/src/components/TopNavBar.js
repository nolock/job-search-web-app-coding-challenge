import React, {Component} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form'
import {NavLink,} from 'react-router-dom'
import {Auth} from '../App';

class TopNavBar extends Component {

    renderForAuthenticatedUser(){
        const userEmail = Auth.user().email;

        const sideLinks = (
            <Nav className="mr-auto">
                <NavLink activeClassName="active"  className="nav-link" to="/">
                    Home
                </NavLink>
                <NavLink activeClassName="active"  className="nav-link" to="/search">
                    Jobs
                </NavLink>
                <NavLink activeClassName="active"  className="nav-link" to="/jobs/new">
                    Post New Ad
                </NavLink>
            </Nav>
        );

        const dropdown = (
            <Form inline>
                <NavDropdown title={userEmail} id="basic-nav-dropdown">
                    <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
                </NavDropdown>
            </Form>
        );

        return [sideLinks, dropdown];
    }

    renderForAnonymousUser(){
        return [null, null];
    }

    render() {
        const user = Auth.user();

        let sideLinks, dropdown;

        if(user === null){
            [sideLinks, dropdown] = this.renderForAnonymousUser();
        } else {
            [sideLinks, dropdown] = this.renderForAuthenticatedUser();
        }

        return (
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="/">Go IT 🎉</Navbar.Brand>
                    {sideLinks}
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
                    {dropdown}
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default TopNavBar;
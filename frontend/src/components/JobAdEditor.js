import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

class JobAdEditor extends Component {
    constructor(props) {
        super(props);

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);

        this.state = {
            isLoading: false,
            isNew: true,
            title: '',
            description: '',
            location: ''
        };

        this.email = '';
    }

    componentDidMount() {
        const {params} = this.props.match;

        if (params.id) {
            this.fetchAndPopulateForm(params.id);
        }
    }

    fetchAndPopulateForm(id) {
        const token = sessionStorage.getItem('apiToken');
        const user = JSON.parse(sessionStorage.getItem('user'));

        fetch(`http://localhost:8000/api/jobs/${id}`, {
            method: "GET",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
                "api-token": token
            },
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);

                    this.setState({
                        isNew: false,
                        title: result.title,
                        description: result.description,
                        location: result.location
                    });
                },
                (error) => {
                    console.log(error);
                    this.props.history.push('/404-not-found')
                }
            );
    }

    handleFormSubmit(event) {
        const form = event.currentTarget;
        console.log(event);

        event.preventDefault();
        event.stopPropagation();

        const data = {
            title: this.state.title,
            description: this.state.description,
            location: this.state.location
        };

        const token = sessionStorage.getItem('apiToken');
        const user = JSON.parse(sessionStorage.getItem('user'));

        this.setState({
            isLoading: true,
        });

        // Set method to create a new Job Ad (POST) or edit (PUT) existing, otherwise
        let method, url;

        if (this.state.isNew) {
            method = 'POST';
            url = `http://localhost:8000/api/users/${user.id}/jobs`;
        } else {
            const {params} = this.props.match;
            method = 'PUT';
            url = `http://localhost:8000/api/users/${user.id}/jobs/${params.id}`;
        }

        fetch(url, {
            method: method,
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
                "api-token": token
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    if (this.state.isNew) {
                        this.props.history.push('/jobs/new/success')
                    } else {
                        console.log('Updated');

                    }
                },
                (error) => {
                    console.log(error);

                    this.setState({
                        validationError: true,
                    });
                }
            ).finally(() => {
            this.setState({
                isLoading: false,
            });
        });
    }

    handleTitleChange(event) {
        const input = event.currentTarget;
        this.setState({title: input.value})
    }

    handleDescriptionChange(event) {
        const input = event.currentTarget;
        this.setState({description: input.value})
    }

    handleLocationChange(event) {
        const input = event.currentTarget;
        this.setState({location: input.value})
    }

    render() {
        const {isLoading, isReady, validationError} = this.state;
        let isDisabled = false;

        if (isLoading === true || isReady === false) {
            // isDisabled = true;
        }

        return (
            <div>
                <Form
                    noValidate
                    onSubmit={this.handleFormSubmit}>
                    <Form.Group controlId="title">
                        <Form.Label>Job Title</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter job title"
                            onChange={this.handleTitleChange}
                            value={this.state.title}
                        />
                    </Form.Group>

                    <Form.Group controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows="3"
                            onChange={this.handleDescriptionChange}
                            value={this.state.description}

                        />
                    </Form.Group>

                    <Form.Group controlId="location">
                        <Form.Label>Location</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Set the job location"
                            onChange={this.handleLocationChange}
                            value={this.state.location}
                        />
                    </Form.Group>

                    <Button
                        variant="primary"
                        type="submit"
                        disabled={isDisabled}
                        onClick={!isLoading ? this.handleFormSubmit : null}
                    >
                        {isLoading ? 'Loading…' : 'Submit'}

                    </Button>
                </Form>
            </div>
        );
    }
}

export default JobAdEditor;
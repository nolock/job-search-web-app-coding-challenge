import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';


class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);

        this.state = {
            isLoading: false,
            isReady: false,
            email: '',
            password: ''
        };

        this.email = '';
    }

    handleFormSubmit(event) {
        const form = event.currentTarget;
        console.log(event);

        const data = {
            email: this.state.email,
            password: this.state.password
        };

        this.setState({isLoading: true}, () => {
            this.props.onSubmit(data);
        });

        event.preventDefault();
        event.stopPropagation();
    }

    handleEmailChange(event) {
        const emailInput = event.currentTarget;
        this.setState({email: emailInput.value})
    }

    handlePasswordChange(event) {
        const passwordInput = event.currentTarget;
        this.setState({password: passwordInput.value})
    }

    render() {
        const {isLoading, isReady} = this.state;
        let isDisabled = false;

        return (
            <div>
                <Form
                    noValidate
                    onSubmit={this.handleFormSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            onChange={this.handleEmailChange}
                            value={this.state.email}
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            onChange={this.handlePasswordChange}
                            value={this.state.password}

                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicChecbox">
                        <Form.Check type="checkbox" label="Check me out"/>
                    </Form.Group>
                    <Button
                        variant="primary"
                        type="submit"
                        disabled={isDisabled}
                        onClick={!isLoading ? this.handleFormSubmit : null}
                    >
                        {isLoading ? 'Loading…' : 'Submit'}

                    </Button>
                </Form>
            </div>
        );
    }
}

export default LoginForm;
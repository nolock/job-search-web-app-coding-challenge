import React, {Component} from 'react';
import Card from 'react-bootstrap/Card';

class JobAdRow extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const title = this.props.data.title;
        const description = this.props.data.description;
        const location = this.props.data.location;
        const locationFilter = `/search?location=${this.props.data.location}`;
        const userName = this.props.data.user.name;
        const created  = this.props.data.created;
        const jobId = this.props.data.id;
        const jobUrl =`/jobs/${jobId}`;

        return (
            <Card style={{width: '18rem'}}>
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Posted by: {userName} at {created}</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Link href={jobUrl} >View</Card.Link>
                    <Card.Link href={locationFilter} >Location: {location}</Card.Link>
                </Card.Body>
            </Card>
        )
    }
}


export default JobAdRow;
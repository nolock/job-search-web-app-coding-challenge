import React, {Component} from 'react';
import Alert from 'react-bootstrap/Alert';

class JobAdNewSuccess extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
                <Alert variant="success">
                    <Alert.Heading>Success!</Alert.Heading>
                    <p>
                        Congratulations, you've successfully created a new Jod Ad!
                    </p>
                    <hr />
                    <div className="d-flex justify-content-end">
                        <a href="/jobs/new" variant="outline-success">
                            Create a new Ad.
                        </a>
                    </div>
                </Alert>
        );
    }
}

export default JobAdNewSuccess;
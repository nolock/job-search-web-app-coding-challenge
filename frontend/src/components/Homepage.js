import React, {Component} from 'react';
import LoginForm from './LoginForm';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import {Auth} from '../App';

class Homepage extends Component {
    constructor(props) {
        super(props);

        this.handleLoginForm = this.handleLoginForm.bind(this);
        this.state = {
            isLoggedIn: false,
            signUpError: false
        };
    }

    componentDidMount() {
        const token = sessionStorage.getItem('apiToken');
        console.log(token);
        if (token !== null) {
            this.setState({isLoggedIn: true});
        }
    }

    handleLoginForm(data) {
        console.log(data);

        fetch('http://localhost:8000/api/auth', {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    sessionStorage.setItem('apiToken', result['api_token']);
                    sessionStorage.setItem('user', JSON.stringify(result['user']));

                    this.setState({isLoggedIn: true});
                },
                (error) => {
                    console.log(error);

                    this.setState({
                        signUpError: true,
                    });
                }
            );
    }

    handleLogoutClick() {
        this.setState({isLoggedIn: false});
    }

    render() {

        const loggedIn = this.state.isLoggedIn;
        const signUpError = this.state.signUpError;

        return (
            <div className="container-fluid">
                <div className="row">

                    <div className="col align-self-center">
                        {!loggedIn ? (<LoginForm showError={signUpError} onSubmit={this.handleLoginForm}/>) :
                            <Card className="text-center">
                                <Card.Header>Welcome back {Auth.user().name }</Card.Header>
                                <Card.Body>
                                    <Card.Title>Special title treatment</Card.Title>
                                    <Card.Text>
                                        With supporting text below as a natural lead-in to additional content.
                                    </Card.Text>
                                    <Button variant="primary" href="/search">Find all jobs</Button>
                                    <Card.Text>
                                        Or
                                    </Card.Text>
                                    <Button variant="primary" href="/search?location=Melbourne">Find jobs in Melbourne.</Button>
                                </Card.Body>
                                <Card.Footer className="text-muted">You have {Auth.user().adsCount} ads. </Card.Footer>
                            </Card>
                        }


                    </div>

                </div>
            </div>
        );
    }
}

export default Homepage;
import React, {Component} from 'react';
import JobAdRow from './JobAdRow';

class JobsSearch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            jobs: [],
            filters: []
        };
    }

    componentDidMount() {
        const token = sessionStorage.getItem('apiToken');
        const user = sessionStorage.getItem('user');

        console.log(token);

        if (token !== null) {
        }

        fetch('http://localhost:8000/api/jobs', {
            method: "GET",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
                "api-token": token
            },
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    this.setState({
                        jobs: result
                    });

                },
                (error) => {
                    console.log(error);

                }
            );
    }

    renderJobAdRow(job) {
        return (
            <JobAdRow data={job}/>
        )
    }

    getFilteredResults() {
        let params = new URLSearchParams(window.location.search);
        let jobs = this.state.jobs;

        if (params.has('location')) {
            const filter = params.get('location');

            jobs = jobs.filter((job) => job.location === filter);
        }

        return jobs;
    }

    render() {
        const items = this.getFilteredResults().map((job) =>
            this.renderJobAdRow(job)
        );

        return (
            <div className="row">
                {items}
            </div>
        );
    }
}

export default JobsSearch;
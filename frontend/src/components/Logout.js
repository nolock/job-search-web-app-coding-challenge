import React, {Component} from 'react';
import {Auth} from '../App';

class Logout extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Auth.purge();
        this.props.history.push('/')
    }

    render() {
        return null;
    }
}

export default Logout;
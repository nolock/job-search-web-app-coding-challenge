import React, {Component} from 'react';
import Alert from 'react-bootstrap/Alert';
import {Auth} from '../App';


class JobAdDelete extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const token = Auth.token();
        const user = Auth.user();
        const {params} = this.props.match;
        const url = `http://localhost:8000/api/users/${user.id}/jobs/${params.id}`;

        fetch(url, {
            method: "DELETE",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
                "api-token": token
            },
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    render() {

        return (
            <Alert variant="success">
                <Alert.Heading>Success!</Alert.Heading>
                <p>
                    Congratulations, your job add has been deleted!
                </p>
                <hr/>
                <div className="d-flex justify-content-end">
                    <a href="/jobs/new" variant="outline-success">
                        Create a new Ad.
                    </a>
                </div>
            </Alert>
        );
    }
}

export default JobAdDelete;
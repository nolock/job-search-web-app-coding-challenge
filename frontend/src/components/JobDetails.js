import React, {Component} from 'react';
import Card from 'react-bootstrap/Card';
import { Auth } from '../App';

class JobDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            job:{
                user:{
                    name:null
                }
            }
        };
    }

    componentDidMount() {
        const token = sessionStorage.getItem('apiToken');
        const user = sessionStorage.getItem('user');
        const { params } = this.props.match;
        console.log(token);

        if(token !== null){
        }

        fetch('http://localhost:8000/api/jobs/' + params.id,{
            method: "GET",
            mode: "cors",
            headers: {
                "Content-Type": "application/json",
                "api-token": token
            },
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    this.setState({
                        job:result
                    });

                },
                (error) => {
                    console.log(error);

                }
            );
    }

    render() {
        console.log(this.state.job)
        const title = this.state.job.title;
        const description = this.state.job.description;
        const userName = this.state.job.user.name;
        const location = this.state.job.location;
        const user = Auth.user();

        let editLink = null;
        let deleteLink = null;

        if( user.id === this.state.job.user.id){
            const editUrl = `/jobs/${this.state.job.id}/edit`;
            const deleteUrl = `/jobs/${this.state.job.id}/delete`;

            editLink  =  <Card.Link href={editUrl}>Edit</Card.Link>
            deleteLink  =  <Card.Link href={deleteUrl}>Delete</Card.Link>
            ;
        }

        return (
            <Card style={{width: '18rem'}}>
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Posted by: {userName}</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text>{location}</Card.Text>
                    <Card.Link href="/search">Back</Card.Link>
                    {editLink}
                    {deleteLink}
                </Card.Body>
            </Card>
        )
    }
}


export default JobDetails;
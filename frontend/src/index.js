import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Homepage from './components/Homepage';
import Logout from './components/Logout';
import JobsSearch from './components/JobsSearch';
import NotFound from './components/NotFound';
import JobDetails from './components/JobDetails';
import JobAdEditor from './components/JobAdEditor';
import JobAdNewSuccess from './components/JobAdNewSuccess';
import JobAdDelete from './components/JobAdDelete';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import TopNavBar from "./components/TopNavBar";

const routing = (
    <Router>
        <div className="container-fluid">
            <TopNavBar/>
            <div className="row">
                <div className="col align-self-start">

                </div>
                <div className="col align-self-center">
                    <Switch>
                        <Route exact path="/" component={Homepage}/>
                        <Route exact path="/logout" component={Logout}/>
                        <Route exact path="/search" component={JobsSearch}/>
                        <Route exact path="/jobs/new" component={JobAdEditor}/>
                        <Route exact path="/jobs/new/success" component={JobAdNewSuccess}/>
                        <Route path="/jobs/:id/edit" component={JobAdEditor}/>
                        <Route path="/jobs/:id/delete" component={JobAdDelete}/>
                        <Route path="/jobs/:id" component={JobDetails}/>
                        <Route component={NotFound}/>
                    </Switch>
                </div>
                <div className="col align-self-end">

                </div>
            </div>
        </div>
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

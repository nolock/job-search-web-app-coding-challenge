# Sidekicker Coding Challenge

## Setup
* Install [Docker](https://docs.docker.com/get-started/)
* Build: `docker-compose build`
* Run: `docker-compose up`
* Execute tasks: `docker-compose exec <container_name> <cmd>`. 
  * e.g. `docker-compose exec coding-challenge-backend php artisan migrate`

## Development
* React frontend: http://localhost
* Lumen backend: http://localhost:8000

# Before running the App...
## Backend DB migration and Data Seeding
1. Login to backend container by issuing following command. 
```bash 
docker exec -it coding-challenge-backend bash 
```
2. Run the DB migration
```bash
 php artisan migrate
```
3. Run PHPUnit tests
```bash
 php vendor/bin/phpunit
 
 ...
OK (11 tests, 33 assertions)
```

## Fron-end
### Following new dependencies have been added (updated both `package.json` and `yarn.lock`):
- "bootstrap": "^4.3.1",
- "react-bootstrap": "^1.0.0-beta.6",
- "react-router-dom": "^5.0.0",  

### Before opening web app [homepage](http://localhost), please, 
check the DB User table to get one of the generated users `email` and `password` to login.

### URLs
- [homepage/login](http://localhost)
- [logout](http://localhost/logout)
- [view all jobs](http://localhost/search)
- [create new post](http://localhost/jobs/new)
- [view post](http://localhost/jobs/1)
- [edit post](http://localhost/jobs/1/edit)

Users can delete only own ads on the ad detail page.
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('auth', [
        'uses' => 'AuthTokenController@token'
    ]);

    $router->get('users', [
        'middleware' => 'auth',
        'uses' => 'UsersController@showAllUsers'
    ]);

    $router->get('users/{id}', [
        'middleware' => 'auth',
        'uses' => 'UsersController@showOneUser'
    ]);

    $router->post('users', [
        'uses' => 'UsersController@create'
    ]);

    $router->delete('users/{id}', [
        'middleware' => 'auth',
        'uses' => 'UsersController@delete'
    ]);

    $router->put('users/{id}', [
        'middleware' => 'auth',
        'uses' => 'UsersController@update'
    ]);

    $router->get('jobs', [
        'middleware' => 'auth',
        'uses' => 'JobAdvertisementController@showAllJobs'
    ]);

    $router->post('users/{userId}/jobs', [
        'middleware' => 'auth',
        'uses' => 'JobAdvertisementController@create'
    ]);

    $router->put('users/{userId}/jobs/{id}', [
        'middleware' => 'auth',
        'uses' => 'JobAdvertisementController@update'
    ]);

    $router->get('jobs/{id}', [
        'middleware' => 'auth',
        'uses' => 'JobAdvertisementController@showOneJob'
    ]);

    $router->delete('users/{userId}/jobs/{id}', [
        'middleware' => 'auth',
        'uses' => 'JobAdvertisementController@delete'
    ]);
});
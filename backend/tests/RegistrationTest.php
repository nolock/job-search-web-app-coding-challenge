<?php

use App\Models\User;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RegistrationTest extends TestCase
{
    use DatabaseTransactions;

    public function testCanRegisterNewUser(): void
    {
        $user = factory('App\Models\User')->make();
        $data = [
            'email' => $user->email,
            'password' => $user->password,
        ];

        $response = $this->json('POST', '/api/users', $data);
        $response->assertResponseStatus(Response::HTTP_CREATED);
        $response->seeJsonStructure([
            'email',
            'id',
        ]);
    }

    public function testCanGetUser(): void
    {
        $user = factory('App\Models\User')->make();
        $data = [
            'email' => $user->email,
            'password' => $user->password,
        ];


        $response = $this->json('POST', '/api/users', $data);
        $response->assertResponseStatus(Response::HTTP_CREATED);

        $jsonResponse = \json_decode($response->response->getContent(), true);
        $userId = $jsonResponse['id'];

        $user = User::findOrFail($userId);

        $headers = [
            'api-token' => $user->api_token
        ];

        $response = $this->json('GET', '/api/users/' . $userId, [], $headers);
        $response->assertResponseStatus(Response::HTTP_OK);

        $response->seeJsonStructure([
            'email',
            'id',
        ]);
    }

    public function testValidationFails()
    {
        $data = [
            'email' => 'invalid_email',
            'password' => '12345',
        ];

        $response = $this->json('POST', '/api/users', $data);
//        var_dump( $response->response->content() );
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->seeJsonEquals([
            'email' => [
                'The email must be a valid email address.'
            ],
            'password' => [
                'The password must be at least 6 characters.'
            ],
        ]);
    }

    public function testCannotRegisterUserWithSameEmail()
    {
        $user = factory('App\Models\User')->make();
        $data = [
            'email' => $user->email,
            'password' => $user->password,
        ];

        $response = $this->json('POST', '/api/users', $data);
        $response->assertResponseStatus(Response::HTTP_CREATED);

        // Try to register user with same email second time
        $data = [
            'email' => $user->email,
            'password' => $user->password,
        ];

        $response = $this->json('POST', '/api/users', $data);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->seeJsonEquals([
            'email' => [
                'The email has already been taken.'
            ]
        ]);
    }

    public function testIfCanDeleteUser()
    {
        $user = factory('App\Models\User')->make();
        $data = [
            'email' => $user->email,
            'password' => $user->password,
        ];

        $response = $this->json('POST', '/api/users', $data);
        $response->assertResponseStatus(Response::HTTP_CREATED);
        $response->seeJsonStructure([
            'email',
            'id',
        ]);

        $jsonResponse = \json_decode($response->response->getContent(), true);
        $userId = $jsonResponse['id'];

        $user = User::findOrFail($userId);

        $headers = [
            'api-token' => $user->api_token
        ];

        $response = $this->json('DELETE', '/api/users/' . $userId, [], $headers);
        $response->assertResponseStatus(Response::HTTP_OK);
    }
}
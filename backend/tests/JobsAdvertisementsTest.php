<?php

use App\Models\User;
use App\Models\JobAdvertisement;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class JobsAdvertisementsTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    public function testDbIsPrePopulatedWithTestData(): void
    {
        $this->assertGreaterThanOrEqual(5, User::count());
        $this->assertGreaterThanOrEqual(5, JobAdvertisement::count());
    }

    public function testUserCanCreateJobAds(): void
    {
        $user = User::all()->random();
        $headers = $this->getRequestHeaders($user);

        $job = factory('App\Models\JobAdvertisement')->make();

        $data = [
            'title' => $job->title,
            'description' => $job->description,
            'location' => $job->location,
        ];

        $response = $this->json(
            'POST',
            "/api/users/{$user->id}/jobs",
            $data,
            $headers
        );

        $response->assertResponseStatus(Response::HTTP_CREATED);

        $response->seeJsonStructure([
            'title',
            'description',
            'location',
        ]);
    }

    public function testUserCanGetAllJobs(): void
    {
        $user = User::all()->random();
        $headers = $this->getRequestHeaders($user);

        $response = $this->json(
            'GET',
            "/api/jobs",
            [],
            $headers
        );

        $response->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUserCanDeleteJobAds(): void
    {
        /** @var User $user */
        $user = User::all()->random();
        $job = $user->jobAdvertisements()->first();

        $headers = $this->getRequestHeaders($user);

        $response = $this->json(
            'DELETE',
            "/api/users/{$user->id}/jobs/{$job->id}",
            [],
            $headers
        );

        $response->assertResponseStatus(Response::HTTP_OK);
    }

    public function testUserEditJobAds(): void
    {
        /** @var User $user */
        $user = User::all()->random();
        $job = $user->jobAdvertisements()->first();

        $headers = $this->getRequestHeaders($user);

        $newTitle = 'Updted title';

        $data = [
            'title' => $newTitle,
            'description' => $job->description,
            'location' => $job->location,
        ];

        $response = $this->json(
            'PUT',
            "/api/users/{$user->id}/jobs/{$job->id}",
            $data,
            $headers
        );

        $response->assertResponseStatus(Response::HTTP_OK);
        $response->seeJsonStructure([
            'title',
            'description',
            'location',
        ]);

        $jobAdEdited = json_decode($response->response->getContent(),true);

        $this->assertEquals($job->id, $jobAdEdited['id']);
        $this->assertEquals($newTitle, $jobAdEdited['title']);
    }

    /**
     * @param $user
     * @return array
     */
    private function getRequestHeaders(User $user): array
    {
        $headers = [
            'api-token' => $user->api_token
        ];
        return $headers;
    }
}

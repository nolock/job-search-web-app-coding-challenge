<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class JobAdvertisement extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'location' => $this->location,
            'created' => $this->created_at->format('D, d M Y'),
            'user' => [
                'id' => $this->user->id,
                'name' => $this->user->name
            ]
        ];
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthTokenController extends Controller
{
    public function token(Request $request)
    {
        $email = $request->get('email');
        $password = Hash::make($request->get('password'));

        /** @var \App\Models\User $user */
        $user =  User::where('email', $email)
//            ->where('password', $password)
            ->first();

        if($user === null){
            return response()->make('', Response::HTTP_FORBIDDEN);
        }

        return response()->json([
            'api_token' => $user->api_token,
            'user' => [
                'id' => $user->id,
                'email' => $user->email,
                'name' => $user->name,
                'adsCount' => $user->jobAdvertisements()->count(),
            ],
        ]);
    }
}

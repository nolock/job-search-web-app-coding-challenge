<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

final class UsersController extends Controller
{
    public function showAllUsers(Request $request)
    {
        return response()->json(User::all());
    }

    public function showOneUser($id)
    {
        return response()->json(User::find($id));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $validator = $this->getValidator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }

        $user = $this->createUser($request->all());

        return response()->json($user, 201);
    }

    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        return response()->json($user, 200);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    /**
     * @param array $data
     * @return Validator
     */
    private function getValidator(array $data): Validator
    {
        return \Illuminate\Support\Facades\Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }

    /**
     * @param array $data
     * @return User
     */
    private function createUser(array $data): User
    {
        return User::create([
            'email' => $data['email'],
            'name' => $data['name'] ?? '',
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60),
        ]);
    }
}
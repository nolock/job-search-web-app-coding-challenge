<?php

namespace App\Http\Controllers;

use App\Models\JobAdvertisement;
use App\Http\Resources;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class JobAdvertisementController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function showAllJobs(Request $request): JsonResponse
    {
        return response()->json(
            Resources\JobAdvertisement::collection(JobAdvertisement::all())
        );
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function showOneJob(int $id): JsonResponse
    {
        return response()->json(
            new Resources\JobAdvertisement(JobAdvertisement::find($id))
        );
    }

    /**
     * @param $userId
     * @param Request $request
     * @return JsonResponse
     */
    public function create($userId, Request $request): JsonResponse
    {
        $user = User::findOrFail($userId);

        $job = $this->createJob($user, $request->all());

        return response()->json(
            new Resources\JobAdvertisement($job),
            201
        );
    }

    public function update($userId, $id, Request $request)
    {
        /** @var JobAdvertisement $jobAdvertisement */
        $jobAdvertisement = JobAdvertisement::findOrFail($id);

        $jobAdvertisement->update($request->all([
            'title',
            'description',
            'location',
        ]));

        return response()->json($jobAdvertisement, 200);
    }

    /**
     * @param int $userId
     * @param int $id
     * @return Response|ResponseFactory
     */
    public function delete(int $userId, int $id)
    {
        JobAdvertisement::find($id)
            ->where('user_id', $userId)
            ->firstOrFail()
            ->delete();

        return response('Deleted Successfully', 200);
    }

    /**
     * @param User $user
     * @param array $data
     * @return JobAdvertisement
     */
    private function createJob(User $user, array $data): JobAdvertisement
    {
        $jobAdvertisement = new JobAdvertisement([
            'title' => $data['title'],
            'description' => $data['description'],
            'location' => $data['location'],
            'user' => $user
        ]);

        $user->jobAdvertisements()->save($jobAdvertisement);

        return $jobAdvertisement;
    }
}

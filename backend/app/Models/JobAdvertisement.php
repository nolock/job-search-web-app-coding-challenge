<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobAdvertisement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'location'
    ];

    public function user()
    {
        return $this->belongsTo('\App\Models\User');
    }
}
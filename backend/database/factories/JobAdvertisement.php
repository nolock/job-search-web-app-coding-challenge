<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\JobAdvertisement::class, function (Faker\Generator $faker) {

    $languages = ['PHP', 'Java', 'C#', 'C++', 'Cobol', 'JavaScript', 'Python', 'Go', 'Ruby'];
    $locations = ['Melbourne', 'Sydney', 'Canberra', 'Brisbane', 'Perth', 'Darwin', 'Hobart'];

    $language = \array_random($languages);
    $location = \array_random($locations);
    return [
        'title' => \sprintf('%s job in %s', $language, $location),
        'description' => $faker->paragraph,
        'location' => $location,
    ];
});

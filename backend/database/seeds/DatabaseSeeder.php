<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Should be this  `$this->call('UsersTableSeeder');` but there is an issue with
        // Composer autoload with non PSR-4 classes. Running composer dump-autoload will fix it
        // but getting it's to run on current Docker image.
        factory(User::class, 10)->create()->each(function (User $user) {
            $user->jobAdvertisements()->saveMany(
                factory(\App\Models\JobAdvertisement::class, 5)->make()
            );
        });
    }
}
